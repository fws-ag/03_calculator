// Package basicmath stellt primitive Funktionen für grundlegende
// mathematische Operationen zur Verfügung.
package basicmath

// Add addiert zwei Zahlen vom Typ float64, gibt einen Ergebniswert vom Typ float64 zurück
func Add(a, b float64) float64 {
	return a + b
}

// Subtract subrahiert zwei Werte vom Typ float64, gibt einen Ergebniswert vom Typ float64 zurück
func Subtract(a, b float64) float64 {
	return a - b
}

// Multiply multipliziert zwei Werte vom Typ float64, gibt einen Ergebniswert vom Typ float64 zurück
func Multiply(a, b float64) float64 {
	return a * b
}

// Divide dividiert zwei Werte vom Typ float64, gibt einen Ergebniswert vom Typ float64 zurück
func Divide(a, b float64) float64 {
	return a / b
}
