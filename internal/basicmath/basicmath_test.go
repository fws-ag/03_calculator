// Package basicmath stellt primitive Funktionen für grundlegende
// mathematische Operationen zur Verfügung.
package basicmath

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAdd(t *testing.T) {

	expectedReturnValue := 3.0
	result := Add(1, 2)

	// Primitive Lösung; "selbstgebautes" assert statement
	if result != expectedReturnValue {
		t.Fatal("Expected", expectedReturnValue, "but got", result)
	}
}

func TestSubtract(t *testing.T) {

	expectedReturnValue := -1.0
	result := Subtract(1, 2)

	if result != expectedReturnValue {
		t.Fatal("Expected", expectedReturnValue, "but got", result)
	}
}

func TestMultiply(t *testing.T) {

	expectedReturnValue := 2.0
	result := Multiply(1, 2)

	// Assert via testify Bibliothek
	assert.Equal(t, expectedReturnValue, result, "a")
}

func TestDivide(t *testing.T) {

	expectedReturnValue := 0.5
	result := Divide(1, 2)

	assert.Equal(t, expectedReturnValue, result, "Elaborate message")
}
