package main

import (
	"fmt"

	"gitlab.com/fws-ag/03_calculator/internal/basicmath"
)

func main() {
	fmt.Println(basicmath.Add(1, 2))
	fmt.Println(basicmath.Subtract(1, 2))
	fmt.Println(basicmath.Multiply(1, 2))
	fmt.Println(basicmath.Divide(1, 2))
}
